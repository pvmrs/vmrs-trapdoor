#include <utility>

#include "trapdoorgenerator.h"

TrapdoorGenerator::TrapdoorGenerator (const Keyset& ks, const string &sizes_path, const string &pairing_path)
{

  vs.importSizes (sizes_path);
  this->ks = ks;

  //Pairing
  FILE *sysParamFile = fopen(pairing_path.c_str(), "r");
  if (sysParamFile == nullptr) {
      cerr <<"Can't open the parameter file " << pairing_path << endl;
      exit(1);
    }

  this->pairing = new Pairing(sysParamFile);
  this->g = G1(*pairing, ks.getGHash ().data(), ks.getGHash ().size());

  verbose = false;
  start = std::chrono::high_resolution_clock::now();

  logmsg("TrapdoorGenerator instantiated");
}

Trapdoor TrapdoorGenerator::generateTrapdoor (vector<string> vw, int k)
{

  string words;
  for (const string& w : vw)
    {
      words += w + ", ";
    }

  logmsg("Generating trapdoor for " + words.substr (0, words.size() - 2) );
  Trapdoor tr = Trapdoor ();

  //first parameters
  tr.first_w =  hmac (ks.getK1 (), vw[0], vs.pi_size).to_string ();
  tr.f_k2_w1 = hmac (ks.getK2 (), vw[0], vs.f_size).to_string ();
  tr.fi_k3_w1 = hmac (ks.getK3 (), vw[0], vs.param_size).to_string ();
  tr.k = k;

  logmsg("W1 = " + to_hex (tr.first_w));

  tr.other_w.clear ();

  //calculates fi_k1(w1+c)
  unordered_map<size_t, Zr> fi_k3_wi;
  for (size_t i = 1; i < vw.size();++i)
    {
      //pi_k1_wi
      tr.other_w[i+1] = hmac (ks.getK1 (), vw[i], vs.pi_size).to_string ();

      //fi_k3_wi
      bitstring wi_pbck3= hmac (ks.getPbcK3 (), vw[i] , vs.zr_size);
      fi_k3_wi[i+1] = from_bs (pairing, wi_pbck3);

      logmsg("W" + to_string(i+1) + " = " + to_hex(tr.other_w[i+1]));
    }

  for (size_t c = 1; c <= vs.max; ++c)
    {

      //paper seys k2, but must be k1
      bitstring fi_k1_w1c = hmac (ks.getPbcK1 (), vw[0] + to_string(c), vs.zr_size); //xid
      Zr zr_fi_k1_w1c = from_bs (pairing, fi_k1_w1c);

      for (size_t i = 1; i < vw.size(); ++i)
        {
          //token[c,i], c is file number (not id), i is word index
          G1 pbc_key (g ^ (fi_k3_wi[i+1] * zr_fi_k1_w1c));
          tr.tokens[{c, i+1}] = pbc_key.toString (false);

          logmsg("Token of W" + to_string(i+1) + ", c = " + to_string(c) + " is " + to_hex(pbc_key.toString (false)));
        }
    }

    return tr;
}

const Keyset &TrapdoorGenerator::get_ks () const
{
  return ks;
}
void TrapdoorGenerator::set_ks (const Keyset &ks)
{
  TrapdoorGenerator::ks = ks;
}
const VMRSSizes &TrapdoorGenerator::get_vs () const
{
  return vs;
}
void TrapdoorGenerator::set_vs (const VMRSSizes &vs)
{
  TrapdoorGenerator::vs = vs;
}

void TrapdoorGenerator::logmsg (const string &msg)
{
  if (verbose) {
      auto stop = std::chrono::high_resolution_clock::now();
      auto duration = duration_cast<nanoseconds>(stop - start);
      cout << duration.count() << "ns: " << msg << endl;
    }
}
bool TrapdoorGenerator::is_verbose () const
{
  return verbose;
}
void TrapdoorGenerator::set_verbose (bool verbose)
{
  this->verbose = verbose;
}