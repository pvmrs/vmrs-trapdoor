#include <iostream>
#include <vector>
#include <fstream>
#include <vmrs/argparse.h>
#include <vmrs/keyset.h>
#include <vmrs/vmrssizes.h>
#include <vmrs/configparams.h>
#include "trapdoorgenerator.h"


using namespace std;

int main (int argc, char* argv[])
{
  ArgumentParser parser("Argument parser for vmrs_key_generator");
  parser.add_argument("-c", "configuration file", false);
  parser.add_argument("-v", "verbose", false);

  try
    {
      parser.parse(argc, argv);
    }
  catch (const ArgumentParser::ArgumentNotFound& ex)
    {
      cout << "Error processing arguments: " << ex.what() << endl;
      return 0;
    }

  ConfigParams* cfg;
  if (parser.exists ("c"))
    {
      cfg = ConfigParams::getInstance(parser.get<string>("c"));
    }
  else
    {
      cfg = ConfigParams::getInstance();
    }

  string keyset_file, search_file, sizes_file, params_file, out_file;
  int keylength, blocksize, k;
  cfg->getParam("SEARCH_FILE",search_file);
  cfg->getParam("KEYSET_FILE",keyset_file);
  cfg->getParam("KEY_LENGTH",keylength);
  cfg->getParam("BLOCKSIZE",blocksize);
  cfg->getParam("SIZES_FILE",sizes_file);
  cfg->getParam("PARAMS_FILE",params_file);
  cfg->getParam("OUT_FILE",out_file);
  cfg->getParam("K",k);



  Keyset ks = Keyset::importKeys (keyset_file, keylength, blocksize);

  TrapdoorGenerator tg(ks, sizes_file, params_file);

  vector<Trapdoor> vec_tr;
  ifstream in_file(search_file);
  if (in_file.is_open())
    {
      string line;
      while (getline(in_file, line))
        {
          istringstream iss(line);
          vector<string> w{istream_iterator<string>{iss}, istream_iterator<string>{}};
          vec_tr.push_back(tg.generateTrapdoor (w, k));
        }
    }
  else
    {
      cout << "Error reading search file." << endl;
    }

  ofstream ofs(out_file);
  boost::archive::text_oarchive ar(ofs);

  ar & vec_tr;
  return 0;
}