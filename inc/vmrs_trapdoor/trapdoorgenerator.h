#ifndef _TRAPDOORGENERATOR_H_
#define _TRAPDOORGENERATOR_H_

#include <iostream>
#include <vector>
#include <chrono>
#include <vmrs/vmrs.h>
#include <vmrs/keyset.h>
#include <vmrs/vmrssizes.h>
#include <vmrs/trapdoor.h>
#include <unordered_map>

using namespace std;
using namespace chrono;
using namespace vmrs;

class TrapdoorGenerator {
 public:
  TrapdoorGenerator(const Keyset &ks, const string &sizes_path, const string &pairing_path);
  Trapdoor generateTrapdoor (vector<string> vw, int k);

  const Keyset& get_ks () const;
  void set_ks (const Keyset &ks);
  const VMRSSizes& get_vs () const;
  void set_vs (const VMRSSizes &vs);

  bool is_verbose () const;
  void set_verbose (bool verbose);
 private:
  Keyset ks;
  Pairing *pairing;
  G1 g;
  VMRSSizes vs;
  bool verbose;
  void logmsg(const string& msg);
  high_resolution_clock::time_point start;
};

#endif //_TRAPDOORGENERATOR_H_
